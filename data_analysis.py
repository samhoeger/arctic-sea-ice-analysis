'''
Description:
This module looks at March and Sepetember every year from 1980 to 2015. The mean of each year and the 
anomaly for each year are found and this informtion is displayed in two different figures.
'''


import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
import statsmodels.api as sm

def get_Mar_Sept_frame():
    '''This function reads in the data_79_15.csv into a dataframe. It then uses that daatframe
    to calculate March mean, March anamoly, September mean, and September anomaly for each year.
    No Parameters
    Returns a dataframe with 1979 to 2015 as its index and a column for each month's mean and anomaly.'''
    dfhw4 = pd.read_csv('data_79_15.csv', index_col=0)
    March_means = dfhw4.ix[:,'0301':'0331'].mean(axis=1)
    March_anomalies = March_means - March_means.mean()
    September_means = dfhw4.ix[:,'0901':'0930'].mean(axis=1)
    September_anomalies = September_means - September_means.mean()
    mean_df = pd.concat([March_means,March_anomalies,September_means,September_anomalies], axis=1)
    mean_df.columns = ['March_means','March_anomalies','September_means','September_anomalies']
    mean_df.index = np.int64(mean_df.index)
    return mean_df
    
def get_ols_parameters(series):
    '''This function takes a series, fits it to a line, and returns the slope, intercept, R2, and the p-value 
    in a list.
    Parameter: a series genrated from the get_Mar_Sept_frame dataframe
    Returns a list with the slope, yintercept, R2, and pvalue'''
    years_array = sm.add_constant(series.index.values)
    model = sm.OLS(series, years_array)
    results = model.fit()
    r2 = results.rsquared
    intercept = results.params['const']
    slope = results.params['x1']
    pvalue = results.pvalues['x1']
    param_list = [slope,intercept, r2, pvalue]
    return param_list

def make_prediction(params, description='x-intercept:', x_name='x',y_name='y', ceiling=False):
    ''' This function takes the paramter list, calculates the x intercept, and prints a message
    with the information, declaring whther or not the result is statistically significant.
    Parameters:
    params: list containing the slope, yintercept, R2, and pvalue
    description: the x intercept value with the month of the year
    x_name: what the x values are representing
    y_name: what the  y values represent
    ceiling: A boolean used to determine whether or not to use the ceiling method
    Returns None
    '''
    x = (-1*params[1])/params[0]
    if ceiling== True:
        x=math.ceil(x)
    print(description, x)
    percent = round(params[2]*100)
    pval= np.float64(round(params[3]*100))
    print(str(percent)+'% of variation in',y_name,'accounted for by',x_name,'(linear model)')
    print('Significance level of results: '+str(pval)+'%')
    if params[3]>.05:
        print('This result is not statistically significant.')
    else:
        print('This result is statistically significant.')

def make_fig_1(March_Sept_frame):
    '''This function takes the get_Mar_Sept_frame dataframe and uses it to create a figure representing
    the means of each month from 1980 to 2015, and the best fit line for the means of March and September.
    '''
    fig=plt.figure()
    ax=March_Sept_frame.loc[:,'March_means'].plot(label='March')
    ax.set_ylabel(r"NH Sea Ice Extent ($10^6$ km$^2$)")
    ax.yaxis.label.set_fontsize(24)
    March_Sept_frame.loc[:,'September_means'].plot(label='September')
    labels=['1980','1985','1990','1995','2000','2005','2010','2015']
    ax.set_xticklabels(labels)
    results = get_ols_parameters(March_Sept_frame['March_means'])
    xs = np.arange(1980,2016)
    ys = results[0] * xs + results[1] 
    plt.plot(xs, ys, linewidth=1)
    result = get_ols_parameters(March_Sept_frame['September_means'])
    x = np.arange(1980,2016)
    y = result[0] * xs + result[1] 
    plt.plot(x, y, linewidth=1)
    
def make_fig_2(March_Sept_frame):
    '''This function takes get_Mar_Sept_frame to generate a figure that graphs a line for
    for the March anomalies and the September anomalies and then graphs the best fit line 
    fo reach month.
    '''
    fig=plt.figure()
    ax=March_Sept_frame.loc[:,'March_anomalies'].plot(label='March')
    ax.set_ylabel(r"NH Sea Ice Extent ($10^6$ km$^2$)")
    ax.yaxis.label.set_fontsize(24)
    plt.title('The Anomaly')
    March_Sept_frame.loc[:,'September_anomalies'].plot(label='September')
    labels=['1980','1985','1990','1995','2000','2005','2010','2015']
    ax.set_xticklabels(labels)
    results = get_ols_parameters(March_Sept_frame['March_anomalies'])
    xs = np.arange(1980,2016)
    ys = results[0] * xs + results[1] 
    plt.plot(xs, ys, linewidth=1)
    result = get_ols_parameters(March_Sept_frame['September_anomalies'])
    x = np.arange(1980,2016)
    y = result[0] * xs + result[1] 
    plt.plot(x, y, linewidth=1)
#==========================================================
def main():
    '''put input function call in main'''
    March_September = get_Mar_Sept_frame()
    make_fig_1(March_September)
    input('enter to cont: ')
    make_fig_2(March_September)
    input('enter to end')
if __name__ == '__main__':
    main()
