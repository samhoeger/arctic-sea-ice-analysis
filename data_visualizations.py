'''
Description:
This module reads a csv file and uses the produced dataframe to analyze the data in graphs.
'''

from hw4 import get_column_labels
import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

def get_2016(date, df):
    ''' This function creates a series for 2016 with the dates as the indices.
    date: month-day string parameter representing the ending date for the series
    df: the dataframe parameter containing the datetime objects and values for 2016
    Returns Series
    '''
    days =(get_column_labels())
    days =days[:days.index(date)+1]
    new = pd.concat([df.iloc[:59], df.iloc[60:]])
    data = new.iloc[:len(days),1]
    data.name = None
    data.index = days
    return data.reindex(days)
    
def extract_fig_1_frame(df1):
    '''This function takes a dataframe with data for every day from 1979 to 2015 and returns a new dataframe
    with the average for each day over the 36 years and 2 standard deviations.
    df1: DataFrame parameter containing information from 1979 to 2015
    Returns: New dataframe with indices of mean and two_s and column labels of the days
    '''
    mean = list(df1.mean())
    
    std = (list(2*df1.std(ddof=1)))
    index = ['mean','two_s']
    data = [mean,std]
    columns = get_column_labels()
    return pd.DataFrame(data,index,columns)
    
def extract_fig_2_frame(df):
    '''This function takes a dataframe with data for every day from 1979 to 2015 and returns a new dataframe
    with the average for each day over each decade.
    df1: DataFrame parameter containing information from 1979 to 2015
    Returns: New dataframe with indices of each decade and column labels of the days
    '''
    mean_1980 = list(df.iloc[1:11].mean())
    mean_1990 = list(df.iloc[11:21].mean())
    mean_2000 = list(df.iloc[21:31].mean())
    mean_2010 = list(df.iloc[31:].mean())
    data = [mean_1980,mean_1990,mean_2000,mean_2010]
    index=['1980s','1990s','2000s','2010s']
    columns=get_column_labels()
    return pd.DataFrame(data,index,columns)

def make_fig_1(df1,df4,y16,date):
    '''This function takes three dataframes and a date string to make a graph of the mean and 2 std devs compared
    to 2012 and 2016.
    Parameters:
    df1: dataframe produced from extract_fig_1_frame
    y16: dataframe produced in hw4
    df4: dataframe produced from data_79_15 csv file
    date: month-day string of ending date
    
    Returns: None
    '''
    ax=df1.loc['mean'].plot(label='mean')
    ax.set_ylabel(r"NH Sea Ice Extent ($10^6$ km$^2$)")
    ax.yaxis.label.set_fontsize(24)
    df4.loc[2012].plot(linestyle='--',label='2012')
    labels=['0101','0220','0411','0531','0720','0908','1028','1217']
    s16=get_2016(date,y16)
    s16.plot(label='2016')
    x = np.arange(0,365)
    y1= df1.loc['mean'].values.astype(float)+df1.loc['two_s'].values.astype(float)
    y2= df1.loc['mean'].values.astype(float)-df1.loc['two_s'].values.astype(float)
    ax.fill_between(x,y1,y2, facecolor='lightgray', edgecolor='lightgray', label='fill_between')
    ax.set_xticklabels(labels)
    ax.legend(['mean','2012','2016',u'\xb1 2 std devs'])
    
def make_fig_2(df1,dfhw4,y16,date):
    '''This function takes three dataframes and a date string to make a graph of each decade and 2016 to compare.
    Parameters:
    df1: dataframe produced from extract_fig_1_frame
    y16: dataframe produced in hw4
    df4: dataframe produced from data_79_15 csv file
    date: month-day string of ending date
    
    Returns: None
    '''
    fig=plt.figure()
    df2=extract_fig_2_frame(dfhw4)
    ax=df2.loc['1980s'].plot(linestyle='--',label='1980s')
    ax.set_ylabel(r"NH Sea Ice Extent ($10^6$ km$^2$)")
    ax.yaxis.label.set_fontsize(24)
    df2.loc['1990s'].plot(linestyle='--',label='1990s')
    df2.loc['2000s'].plot(linestyle='--',label='2000s')
    df2.loc['2010s'].plot(linestyle='--',label='2010s')
    labels=['0101','0220','0411','0531','0720','0908','1028','1217']
    s16=get_2016(date,y16)
    s16.plot(label='2016')
    ax.set_xticklabels(labels)
    ax.legend(['1980s','1990s','2000s','2010s','2016'], loc=3)
#==========================================================
def main():
    '''put input function call in main'''
    
    df16 = pd.read_csv('NH_seaice_extent_nrt2.csv', skiprows=[0,1], usecols=[0,1,2,3], parse_dates=[[0,1,2]], header=None)
    dfhw4 = pd.read_csv('data_79_15.csv', index_col=0)
    frame1 = extract_fig_1_frame(dfhw4)
    make_fig_1(frame1,dfhw4,df16,'0925')
    input('Enter to continue: ')
    make_fig_2(frame1,dfhw4,df16,'0925')
    input('Enter to continue: ')
if __name__ == '__main__':
    main()
