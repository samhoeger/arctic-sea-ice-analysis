'''
Description:
This assignment  load the data from both files into DataFrames, combine them, and write a new 
file to disk covering 1979-2015 in csv format, but with the data cleaned and reformatted.
'''


import pandas as pd
from ftplib import FTP
import datetime
import numpy as np
from datetime import datetime

def read_data():
    '''
    Load NH_seaice_extent_final.csvand NH_seaice_extent_nrt.csvinto DataFramesand concatenate them.
    df1=dataframe created by reading in NH_seaice_extent_final
    df2= dataframe created by reading in NH_seaice_extent_nrt
    Return the new DataFrame
    '''
    df1=pd.read_csv('NH_seaice_extent_final.csv', skiprows=[0,1], usecols=[0,1,2,3], parse_dates=[[0,1,2]], header=None)
    df2=pd.read_csv('NH_seaice_extent_nrt.csv', skiprows=[0,1], usecols=[0,1,2,3], parse_dates=[[0,1,2]], header=None)
    frames=[df1,df2]
    new=pd.concat(frames)
    return new
def extract_ts(df):
    '''
    This function takes the DataFramecreated in read_dataas an argument and returns a Series.  
    The index of the Serieshas the same values as the 0_1_2column of the DataFrame, but reindexed
    so that all dates from 10/26/1978 to 6/19/2016 are included.  The values are the values in the column labeled 3, where available, NaNwhere not.
    df: dataframe from the read_data function from a read csv file
    Returns a series
    '''
    df.index=df['0_1_2'].values
    s=df[3]
    index2=pd.date_range(s.index[0],s.index[-1])
    new=s.reindex(index2)
    return new
def  clean_data(s):
    '''
    This function takes the Seriescreated in extract_tsand alters it in place by filling in the 
    missing data.  For slots that have data for the previous and following days, replace NaN with
    the mean of those two days.  For the extended period of missing data that begins in late 1987 
    and ends in early 1988, replace NaNwith the mean of the previous and following years on the same 
    day of the year.
    s: argument of a series created in extract_ts
    Returns None
    '''
    for i in range(len(s)):
        if pd.isnull(s[i]):
            s[i]=(s[i-1]+s[i+1])/2
    for i in range(len(s)):
        if pd.isnull(s[i]):
            s[i]=(s[i+366]+s[i-365])/2
def get_column_labels():
    '''
    This funcion generates and retruns a list of strings representing each day of the year.
    No arguments
    days: list created in the function
    Returns days list
    '''
    days=[]
    for i in range(1,13):
        if i in [1,3,5,7,8,10,12]:
            last=32
        if i in [4,6,9,11]:
            last=31
        if i==2:
            last=29
        for j in range(1,last):
            days.append(str(i).zfill(2)+str(j).zfill(2))
    return days
def extract_df(cs):
    '''
    This function takes the cleaned Seriesas its argument and creates and returns a new DataFrame.
    This DataFramewill have the years (ints) from 1979 to 2015 as row labels and the strings from
    get_column_labelsas column labels.  Create an empty DataFramewith those labels.  Then fill in 
    the data using the appropriate values from the Seriee.
    cs: a series from the clean series function
    Returns a new Dataframe
    '''
    years=[]
    for i in range(1979, 2016):
        years.append(i)
    columns=get_column_labels()
    df=pd.DataFrame(index=years,columns=columns)
    for year in df.index:
        for mmdd in df.columns:
            df.ix[year,mmdd]=cs[datetime(year,int(mmdd[:2]),int(mmdd[2:]))]
    return df
#==========================================================
def main():
    '''
    Combines the above functions to read in the data we want, clean it, and store it to disk
    in the file data_79_15.csv
    '''
    datafrm=read_data()
    series=extract_ts(datafrm)
    clean_data(series)
    new=extract_df(series)
    print(new)
    new.to_csv('data_79_15.csv')
    
if __name__ == '__main__':
    main()
